<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18/06/2018
 * Time: 5:05 CH
 */

namespace Drupal\hello\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class Home extends FormBase
{

    public function getFormId()
    {
        // TODO: Implement getFormId() method.
        return 'home';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        // TODO: Implement buildForm() method.
        $form['question_type_select'] = [
            // This is our select dropdown.
            '#type' => 'select',
            '#title' => $this->t('Question style'),
            // We have a variety of form items you can use to get input from the user.
            '#options' => [
                'Choose question style' => 'Choose question style',
                'Multiple Choice' => 'Multiple Choice',
                'True/False' => 'True/False',
                'Fill-in-the-blanks' => 'Fill-in-the-blanks',
            ],
            // The #ajax section tells the AJAX system that whenever this dropdown
            // emits an event, it should call the callback and put the resulting
            // content into the wrapper we specify. The questions-fieldset-wrapper is
            // defined below.
            '#ajax' => [
                'wrapper' => 'questions-fieldset-wrapper',
                'callback' => '::promptCallback',
            ],
        ];
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // TODO: Implement submitForm() method.
    }
}