<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14/06/2018
 * Time: 3:58 CH
 */

namespace Drupal\hello\Controller;



use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;


class HelloController extends ControllerBase
{

    public function home()
    {
        $config = \Drupal::config('hello.settings');
        $page_title = $config->get('hello.page_title');
        $source_text = $config->get('hello.source_text');
        $element['#title'] = t('Title');
        $element['#source_text'] = $source_text;
        $element['#theme'] = 'hello';

        return $element;
    }

    public function login()
    {
        return array(
            '#type' => 'markup',
            '#markup' => t('Login Page!'),
        );
    }
}